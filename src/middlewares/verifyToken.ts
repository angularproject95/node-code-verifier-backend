import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv'

dotenv.config();

const secretkey = process.env.SECRETKEY || 'SECRETKEY'
/**
 * 
 * @param {Request} req Original request previous middleware of verification JWT
 * @param {Response} res Request to verification of JWT
 * @param  {NextFunction} next Next Function to be executed
 * @returns Errors of verification or next execution
 */
export const verifyToken = (req: Request, res: Response, next: NextFunction) => {

    // * Check HEADER from Request for 'x-access-token'
    let token: any = req.headers['x-access-token'];

    // * Verify if token
    if (!token) {
        return res.status(403).send({
            authenticationError: 'Missing JWT in request',
            message: 'Not Autorized to consume this endpoint'
        });
    }

    //TODO: pass secret key
    // * Verify the token obtained
    jwt.verify(token, secretkey, (err: any, decoded: any) => {
        if (err) {
            return res.status(500).send({
                authenticationError: 'JWT verification failed',
                message: 'Failed to Verify JWT token in request'
            });
        }

        // TODO: Pass something to next request (id of user || other info)

        // * Execute Next Function -> Protected Routes will be executed
        next();
    });
}