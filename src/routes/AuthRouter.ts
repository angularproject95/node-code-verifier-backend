import express, { Request, Response } from "express";
import { IUser } from '../domain/interfaces/IUser.interface';
import { AuthController } from "../controller/AuthController";
import { IAuth } from "../domain/interfaces/IAuth.inteface";
import bcrypt from 'bcrypt';

// * Middleware Verify Token
import { verifyToken } from '../middlewares/verifyToken';
// * 
import bodyParser from 'body-parser';

let jsonParser = bodyParser.json();

// * Router from express
let authRouter = express.Router();

authRouter.route('/register')
    .post(jsonParser, async (req: Request, res: Response) => {
        let { name, email, password, age } = req?.body;
        let hashedPassword = '';

        if (name && email && password && age) {
            // * Obtain the password in request and cypher
            hashedPassword = bcrypt.hashSync(password, 8)

            let newUser: IUser = {
                name,
                email,
                password: hashedPassword,
                age,
                katas: []
            }

            // Obtain Response
            const controller: AuthController = new AuthController;
            const response: any = await controller.registerUser(newUser);
            return res.status(200).send(response);
        } else {
            return res.status(400).send({
                message: '[ERROR User Data missing]: No user can be registered'
            });
        }
    });

authRouter.route('/login').post(jsonParser, async (req: Request, res: Response) => {

    let { email, password } = req?.body;

    if (email && password) {

        // * Controller Instance to execute method
        const controller: AuthController = new AuthController;

        //TODO: use IAuth
        let auth: IAuth = {
            email,
            password
        }
        // * Obtain Response
        const response: any = await controller.loginUser(auth);

        // * Send to the client the response which include the JWT to authorize requests
        return res.status(200).send(response);
    } else {
        return res.status(400).send({
            message: '[ERROR User Data missing]: No user can be registered'
        });
    }
});

// * Route Protected by VERIFY TOKEN middleware
authRouter.route('/me').get(verifyToken, async (req: Request, res: Response) => {
    // * Obtain the ID of user to check it's data
    let id: any = req?.query?.id;

    if (id) {
        // * Controller: Auth Controller
        const controller: AuthController = new AuthController();
        // * Obtain response from controller
        let response: any = await controller.userData(id);

        // * If User is authorized
        res.status(200).send(response);
    } else {
        return res.status(401).send({
            message: `You are not Authorized to perform this action`
        })
    }
})

export default authRouter