import express, { Request, Response } from "express";
import { UserController } from "../controller/UsersController";
import { LogInfo } from "../utils/logger";
import { verifyToken } from "../middlewares/verifyToken";
// * Body Parser to read BODY from requests
import bodyParser from 'body-parser';

let jsonParser = bodyParser.json();


// Router from express
let usersRouter = express.Router();

// http://localhost:8000/api/users?id=62a3c15b2536326f2f56974b
usersRouter.route('/')
    .get(verifyToken, async (req: Request, res: Response) => {
        let id: any = req?.query?.id;

        // * Pagination
        let page: any = req?.query?.page || 1
        let limit: any = req?.query?.limit || 10;

        LogInfo(`Query Params: ${id}`);

        const controller: UserController = new UserController();
        const response: any = await controller.getUsers(page, limit, id);

        return res.status(200).send(response);
    })
    //* DELETE: 
    .delete(verifyToken, async (req: Request, res: Response) => {
        let id: any = req?.query?.id;
        LogInfo(`Query Params: ${id}`);
        const controller: UserController = new UserController();
        const response: any = await controller.deleteUser(id);
        return res.status(200).send(response);
    })
    .put(verifyToken, jsonParser, async (req: Request, res: Response) => {
        let id: any = req?.body?.id;
        let name: any = req?.body?.name;
        let email: any = req?.body?.email;
        let age: any = req?.body?.age;

        LogInfo(`Query Params: ${id}, ${name}, ${email}, ${age}`);

        const controller: UserController = new UserController();
        let user = { name: name, email: email, age: age }

        const response: any = await controller.updateUser(id, user);
        return res.status(200).send(response);
    });

usersRouter.route('/katas')
    .get(verifyToken, async (req: Request, res: Response) => {
        let id: any = req?.query?.id;

        // * Pagination
        let page: any = req?.query?.page || 1
        let limit: any = req?.query?.limit || 10;

        const controller: UserController = new UserController();
        const response: any = await controller.getKatas(page, limit, id);

        return res.status(200).send(response);
    })

// Export Hello Router
export default usersRouter;
