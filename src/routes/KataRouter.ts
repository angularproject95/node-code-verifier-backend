import express, { Request, Response } from "express";
import { KatasController } from "../controller/KatasController";
import { LogInfo } from "../utils/logger";
import { verifyToken } from "../middlewares/verifyToken";
// * Body Parser to read BODY from requests
import bodyParser from 'body-parser';
import { IKata, KataLevel } from "../domain/interfaces/IKata.interface";

let jsonParser = bodyParser.json();


// Router from express
let katasRouter = express.Router();

// http://localhost:8000/api/users?id=62a3c15b2536326f2f56974b
katasRouter.route('/')
    .get(verifyToken, async (req: Request, res: Response) => {
        let id: any = req?.query?.id;

        // * Pagination
        let page: any = req?.query?.page || 1
        let limit: any = req?.query?.limit || 10;

        LogInfo(`Query Params: ${id}`);

        const controller: KatasController = new KatasController();
        const response: any = await controller.getKatas(page, limit, id);

        return res.status(200).send(response);
    })
    .delete(verifyToken, async (req: Request, res: Response) => {
        let id: any = req?.query?.id;
        LogInfo(`Query Params: ${id}`);
        const controller: KatasController = new KatasController();
        const response: any = await controller.deleteKata(id);
        return res.status(200).send(response);
    })
    .put(jsonParser, verifyToken, async (req: Request, res: Response) => {

        let id: any = req?.query?.id;
        
        let name: string = req?.body?.name;
        let description: string = req?.body?.description || 'Default Description';
        let level: KataLevel = req?.body?.level || KataLevel.BASIC;
        let intents: number = req?.body?.intents || 0;
        let stars: number = req?.body?.stars || 0;
        let creator: string = req?.body?.creator;
        let solution: string = req?.body?.solution|| 'Default Solution';
        let participants: string[] = req?.body?.participants || ['a'];

        if(name && description && level && intents>= 0 && stars >= 0 && creator && solution && participants.length >= 0){
            const controller: KatasController = new KatasController();
            let kata: IKata = {
                name: name,
                description: description,
                level: level,
                intents: intents,
                stars:stars,
                creator:creator,
                solution:solution,
                participants:participants,
            }
            const response: any = await controller.updateKata(id, kata);
            return res.status(200).send(response);
        }else{
            return res.status(400).send({
                message: `[ERROR] Updating Kata. Yo need to send all attrs of Kata to update it`
            })
        }
    }).post(jsonParser, verifyToken, async(req: Request, res: Response) => {
        let name: any = req?.body?.name;
        let description: any = req?.body?.description || 'Default Description';
        let level: KataLevel = req?.body?.level || KataLevel.BASIC;
        let intents: number = req?.body?.intents || 0;
        let stars: number = req?.body?.stars || 0;
        let creator: string = req?.body?.creator;
        let solution: string = req?.body?.solution|| 'Default Solution';
        let participants: string[] = req?.body?.participants || ['a'];

        if(name && description && level && intents>= 0 && stars >= 0 && creator && solution && participants.length >= 0){
            const controller: KatasController = new KatasController();
            let kata: IKata = {
                name: name,
                description: description,
                level: level,
                intents: intents,
                stars:stars,
                creator:creator,
                solution:solution,
                participants:participants,
            }
            const response: any = await controller.createKata(kata);
            return res.status(200).send(response);
        }else{
            return res.status(400).send({
                message: `[ERROR] Creating Kata. Yo need to send all attrs of Kata to create it`
            })
        }

    });

// Export Hello Router
export default katasRouter;
