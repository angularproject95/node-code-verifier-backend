import { userEntity } from "../entities/User.entity";

import { LogSuccess, LogError } from "../../utils/logger";
import { IUser } from "../interfaces/IUser.interface";
import { IAuth } from "../interfaces/IAuth.inteface";
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

// * Enviroment variables
import dotenv from 'dotenv';
import { kataEntity } from "../entities/Kata.entity";
import { IKata } from "../interfaces/IKata.interface";
import mongoose from "mongoose";
// import { UsersResponse } from "../types/UsersResponse.type";

dotenv.config();

//* Obtain Secret key to generate JWT
const secret = process.env.SECRETKEY || 'SECRETKEY'

// CRUD

/**
 * Method to obtain all Users from Collection "Users" in Mongo Server
 */
export const getAllUsers = async (page: number, limit: number): Promise<any[] | undefined> => {
    try {
        let userModel = userEntity();

        let response: any = {};

        // * Search all users (using pagination)
        await userModel.find({ isDeleted: false })
            .select('name email age katas')
            .limit(limit)
            .skip((page - 1) * limit)
            .exec().then((users: IUser[]) => {
                response!.users = users;
            });

        // * Count total documents in collection "Users"
        await userModel.countDocuments().then((total: number) => {
            response!.totalPages = Math.ceil(total / limit);
            response!.currentPage = page;
        });

        return response;
    } catch (error) {
        LogError(`[ORM ERROR] Getting All Users: ${error}`);
    }
}
/**
 * Get User By Id
 */

export const getUserById = async (id: string): Promise<any | undefined> => {
    try {
        let userModel = userEntity();

        // * Search User By ID
        return await userModel.findById(id).select('name email age katas');
    } catch (error) {
        LogError(`[ORM ERROR] Getting Users By ID: ${error}`);
    }
}

/**
 * Delete User By Id
 */

export const deleteUserById = async (id: string): Promise<any | undefined> => {
    try {
        let userModel = userEntity();
        // * Delete User By ID
        return await userModel.deleteOne({ _id: id })
    } catch (error) {
        LogError(`[ORM ERROR] Delete Users By ID: ${error}`);
    }
}

/**
 * Update User By Id
 */

export const updateUserById = async (id: string, user: any): Promise<any | undefined> => {
    try {
        let userModel = userEntity();

        // * Update error
        return await userModel.findByIdAndUpdate(id, user);
    } catch (error) {
        LogError(`[ORM ERROR] Updating user ${id}: ${error}`);
    }
}

// * Register User
export const registerUser = async (user: IUser): Promise<any | undefined> => {
    try {
        let userModel = userEntity();

        // Create / INsert new User
        return await userModel.create(user);
    } catch (error) {
        LogError(`[ORM ERROR] Creating user: ${error}`);
    }
}

// * Login User
export const loginUser = async (auth: IAuth): Promise<any | undefined> => {
    try {
        let userModel = userEntity();

        let userFound: IUser | undefined = undefined;
        let token = undefined;

        await userModel.findOne({ email: auth.email }).then((user: IUser) => {
            userFound = user;

        }).catch((error) => {
            console.error(`[ERROR Authentication in ORM]: User Not Found`)
            throw new Error(`[ERROR Authentication in ORM]: User Not Found: ${error}`)
        });

        let validPassword = bcrypt.compareSync(auth.password, userFound!.password);

        // * Check if password is valid(compare with bcrypt)
        if (!validPassword) {
            // TODO: Not Authorized (401)
            console.error(`[ERROR Authentication in ORM]: Password Not Valid`)
            throw new Error(`[ERROR Authentication in ORM]: Password Not Valid`);
        }

        // * Generate our JWT
        token = jwt.sign({ email: userFound!.email }, secret, {
            expiresIn: "2h"
        })

        return {
            user: userFound,
            token: token
        }

    } catch (error) {
        LogError(`[ORM ERROR] Loggin user: ${error}`);
    }
}

// * Logout User
export const logoutUser = async (): Promise<any | undefined> => {
    try {

    } catch (error) {

    }
}

export const getKatasFromUser = async (page: number, limit: number, id:string ): Promise<any[] | undefined> => {
    try {
        let userModel = userEntity();
        let katasModel = kataEntity();

        let katasFound: IKata[] = [];

        let response: any = {
            katas: []
        };

        console.log('User ID', id);

        await userModel.findById(id).then(async (user: IUser) => {
            response.user = user.email;

            // console.log('Katas from User', user.katas);

            // Create types to search
            let objectIds:mongoose.Types.ObjectId[]  = [];
            user.katas.forEach((kataID: string) => {
                let objectID = new mongoose.Types.ObjectId(kataID);
                objectIds.push(objectID);
            });

            await katasModel.find({"_id": {"$in": objectIds }}).then((katas: IKata[]) => {
                katasFound = katas;
            });

        }).catch((error) => {
            LogError(`[ORM ERROR]: Obtaining User: ${error}`);
        })

        response.katas = katasFound;

        return response;

    } catch (error) {
        LogError(`[ORM ERROR]: Getting All Users: ${error}`);
    }
}




