import { Get, Delete, Put, Post, Route, Tags, Query } from 'tsoa';
import { IAuthController } from './interfaces'
import { LogSuccess, LogError, LogWarning } from '../utils/logger';
import { IAuth } from '../domain/interfaces/IAuth.inteface';
import { IUser } from '../domain/interfaces/IUser.interface';

// * ORM - Users Collection
import { registerUser, loginUser, logoutUser, getUserById } from '../domain/orm/User.orm';
import { AuthResponse, ErrorResponse } from './types';


@Route("/api/auth")
@Tags("AuthController")

export class AuthController implements IAuthController {

    @Post("/register")
    public async registerUser(user: IUser): Promise<any> {

        let response: any = '';

        if (user) {
            LogSuccess(`[/api/auth/register] Register New User ${user.email}`)
            await registerUser(user).then((r) => {
                LogSuccess(`[/api/auth/register] Created User ${user.email}`)
                response = {
                    message: `User Created successfully: ${user.name}`
                }
            });
        } else {
            LogWarning("[/api/auth/register] Register needs User Entity")
            response = {
                message: 'User not Registered: Please, provide a User Entity to create one'
            }
        }
        return response
    }

    @Post("/login")
    public async loginUser(auth: IAuth): Promise<any> {

        let response: AuthResponse | ErrorResponse | undefined;

        if (auth) {
            LogSuccess(`[/api/auth/login] Loggin User ${auth.email}`)

            let data = await loginUser(auth)

            response = {
                token: data.token, // JWT generated for logged in user
                message: `Welcome, ${data.user.name}`,
            }
        } else {
            LogWarning("[/api/auth/login] Login needs Auth Entity (email && password)")
            response = {
                error: '[AUTH ERROR]: Email & Password are needed',
                message: 'Please, provide a email && password to login',
            }
        }

        return response
    }

    /**
     * Endpoint to retreive the User in the collection "Users" of DB
     * Middleware: Validate JWT
     * in headers you must add the x-access-token with a valid JWT
     * @param {string} id Id of user to retreive (optional)
     * @returns All user o user found by ID
     */

    @Get('/me')
    public async userData(@Query()id: string):Promise<any> {
        let response: any = '';

        if(id){
            LogSuccess(`[/api/me] Get User Data By ID: ${id}`);
            response = await getUserById(id)
        }

        return response;
    }
    @Post("/logout")
    public async logoutUser(auth: any): Promise<any> {
        let response: any = '';

        //TODO: Authenticate user and return a JWT
        throw new Error('Method not implemented.');
    }

}