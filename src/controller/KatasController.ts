import { Get, Delete, Put, Post, Route, Tags, Query } from 'tsoa';
import { IKataController } from './interfaces'
import { LogSuccess, LogError, LogWarning } from '../utils/logger';

// * ORM - Users Collection

import { updateKataById, deleteKataById, getAllKatas, getKataById, createKata } from '../domain/orm/Kata.orm';
import { IKata } from '../domain/interfaces/IKata.interface';


@Route("/api/katas")
@Tags("KatasController")

export class KatasController implements IKataController {
    /**
     * Endpoint to retreive the Katas in the collection "Katas" of DB
     * @param {string} id Id of kata to retreive (optional)
     * @returns All kata o kata found by ID
     */
    @Get("/")
    public async getKatas(@Query() page: number, @Query() limit: number, @Query() id?: string): Promise<any> {

        let response: any = '';

        if (id) {
            LogSuccess(`[/api/katas] Get Kata by ID ${id}`)
            response = await getKataById(id);
        } else {
            LogSuccess("[/api/katas] Get All Katas Request")
            response = await getAllKatas(page, limit);
        }
        return response
    }

    @Post("/")
    public async createKata(kata: IKata): Promise<any> {
        let response: any = '';

        if (kata) {
            LogSuccess(`[/api/katas] Create New Kata ${kata.name}`)
            await createKata(kata).then((r) => {
                LogSuccess(`[/api/katas] Created Kata ${kata.name}`)
                response = {
                    message: `Kata Created successfully: ${kata.name}`
                }
            });
        } else {
            LogWarning("[/api/katas] Register needs Kata Entity")
            response = {
                message: 'Kata not Registered: Please, provide a Kata Entity to create one'
            }
        }
        return response
    }

    @Put("/")
    public async updateKata(id: string, kata: IKata): Promise<any> {
        let response: any = '';

        if (id) {
            LogSuccess(`[/api/katas] Update User By ID ${id}`)
            await updateKataById(id, kata).then((r) => {
                response = {
                    message: `Katas with id ${id} update successfully`
                }
            });
        } else {
            LogWarning("[/api/katas] Update Kata Request WITHOUT ID")
            response = {
                message: 'Please, provide an ID to update an existing kata'
            }
        }
        return response
    }

    /**
   * Endpoint to delete the Katas in the collection "Katas" of DB
   * @param {string} id Id of kata to delete (optional)
   * @returns message informing if deletion was correct
   */
    @Delete("/")
    public async deleteKata(@Query() id?: string): Promise<any> {

        let response: any = '';

        if (id) {
            LogSuccess(`[/api/katas] Delete Kata By ID ${id}`)
            await deleteKataById(id).then((r) => {
                response = {
                    message: `Kata with id ${id} deleted successfully`
                }
            });
        } else {
            LogWarning("[/api/katas] Delete Kata Request WITHOUT ID")
            response = {
                message: 'Please, provide an ID to remove from database'
            }
        }

        return response
    }
}