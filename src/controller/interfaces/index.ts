import { IKata } from "../../domain/interfaces/IKata.interface";
import { IUser } from "../../domain/interfaces/IUser.interface";
import { BasicResponse } from "../types";

export interface IHelloController {
    getMessage(name?: string): Promise<BasicResponse>
}

export interface IUserController {
    // * Read all users from database || get User By Id
    getUsers(page: number, limit: number, id?: string): Promise<any>
    // * Get Katas of User
    getKatas(page: number, limit: number, id?:string): Promise<any>
    // * Update User
    updateUser(id: string, user: any): Promise<any>
    // * Delete User By Id
    deleteUser(id?: string): Promise<any>

}

export interface IAuthController{
    // * Register User
    registerUser(user: IUser): Promise<any>;
    // * Login User
    loginUser(auth: any): Promise<any>;
}

export interface IKataController {
    // * Read all Katas from database || get User By Id
    getKatas(page: number, limit: number, id?: string): Promise<any>
    // * Update Kata
    updateKata(id: string, kata: IKata): Promise<any>
    // * Delete Kata By Id
    deleteKata(id?: string): Promise<any>

}