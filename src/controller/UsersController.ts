import { Get, Delete, Put, Post, Route, Tags, Query } from 'tsoa';
import { IUserController } from './interfaces'
import { LogSuccess, LogError, LogWarning } from '../utils/logger';

// * ORM - Users Collection

import { updateUserById, deleteUserById, getAllUsers, getUserById, getKatasFromUser } from '../domain/orm/User.orm';


@Route("/api/users")
@Tags("UserController")

export class UserController implements IUserController {
    /**
     * Endpoint to retreive the Users in the collection "Users" of DB
     * @param {string} id Id of user to retreive (optional)
     * @returns All user o user found by ID
     */
    @Get("/")
    public async getUsers(@Query() page: number, @Query() limit: number, @Query() id?: string): Promise<any> {

        let response: any = '';

        if (id) {
            LogSuccess(`[/api/users] Get User by ID ${id}`)
            response = await getUserById(id);
        } else {
            LogSuccess("[/api/users] Get All Users Request")
            response = await getAllUsers(page, limit);
            // TODO: remove passwords from response
        }

        return response
    }

    /**
     * Endpoint to delete the Users in the collection "Users" of DB
     * @param {string} id Id of user to delete (optional)
     * @returns message informing if deletion was correct
     */
    @Delete("/")
    public async deleteUser(@Query() id?: string): Promise<any> {

        let response: any = '';

        if (id) {
            LogSuccess(`[/api/users] Delete User By ID ${id}`)
            await deleteUserById(id).then((r) => {
                response = {
                    message: `User with id ${id} deleted successfully`
                }
            });
        } else {
            LogWarning("[/api/users] Delete User Request WITHOUT ID")
            response = {
                message: 'Please, provide an ID to remove from database'
            }
        }

        return response
    }

    @Put("/")
    public async updateUser(id: string, user: any): Promise<any> {
        let response: any = '';

        if (id) {
            LogSuccess(`[/api/users] Update User By ID ${id}`)
            await updateUserById(id, user).then((r) => {
                response = {
                    message: `User with id ${id} update successfully`
                }
            });
        } else {
            LogWarning("[/api/users] Update User Request WITHOUT ID")
            response = {
                message: 'Please, provide an ID to update an existing user'
            }
        }

        return response
    }
    @Get("/katas")
    public async getKatas(@Query() page: number, @Query() limit: number, @Query() id: string): Promise<any> {
        let response: any = '';

        if (id) {
            LogSuccess(`[/api/users/katas] Get Katas from User By ID ${id}`)
            response = await getKatasFromUser(page, limit, id);

        } else {
            LogWarning("[/api/users/katas] User For katas without ID")
            response = {
                message: 'ID from user is needed'
            }
        }
        return response
    }
}